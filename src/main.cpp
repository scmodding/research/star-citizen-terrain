#include <iostream>
#include <filesystem>
#include <fstream>
#include <string>

#include "SOIL2.h"

void calcContourColor(uint16_t i, unsigned char absRGB, 
                                  unsigned char& r, 
                                  unsigned char& g, 
                                  unsigned char& b,
                                  bool mapGray = false) {
    if(mapGray) {
        r = absRGB;
        g = absRGB;
        b = absRGB;
    } else { 
        switch((i/21846) % 3) {
            case 0: // Red primary
                r = absRGB*3;
                g = (i > (i/21846)/2) ? (255 - (absRGB-128) * 2)/256 : 0;
                b = (i < (i/21846)/2) ? (255 - (absRGB) * 2)/256 : 0;
                break;
            case 1: // Green primary
                r = (absRGB < 128) ? (255 - (absRGB) * 2)/256 : 0;
                g = ((absRGB-((256/3))))*3;
                b = (absRGB > 128) ? (255 - (absRGB-128) * 2)/256 : 0;
                break;
            case 2: // Blue primary
                r = (absRGB > 128) ? (255 - (absRGB-128) * 2)/256 : 0;
                g = (absRGB < 128) ? (255 - (absRGB) * 2)/256 : 0;
                b = ((absRGB-((256/3)*2)))*3;
                break;
        }
    }
}

int main(int argc, char** argv) {
    std::string filename;
    std::string outname;

    if(argc == 2) {
        filename = std::string(argv[1]);
        outname = "./";
    } else if(argc == 3) {
        filename = std::string(argv[1]);
        outname = std::string(argv[2]);
    } else {
        std::cout << "Usage: " << std::endl
                  << "./scterrain filename" << std::endl
                  << "or ./scterrain filename outname" << std::endl;
        return 1;
    }

    std::filesystem::path fnp;
    std::filesystem::path onp;

    // Check if current path (not technically correct, but I don't care c:)
    if(filename == "./" || filename == ".\\") { fnp = std::filesystem::current_path(); }  
    else { fnp = std::filesystem::path(filename); }
    if(outname == "./" || outname == ".\\") { onp = std::filesystem::current_path(); }
    else { onp = std::filesystem::path(outname); }

    std::filesystem::directory_entry fnd(fnp);
    std::filesystem::directory_entry ond(onp);
    
    if(!fnd.exists()) {
        std::cerr << "Input file does not exist!" << std::endl;
        std::abort();
    }

    if(!ond.exists()) {
        try {
            std::filesystem::create_directories(ond.path());
        } catch(std::filesystem::filesystem_error &e) {
            std::cerr << e.what() << std::endl;
            std::abort();
        }
    } else if(ond.is_regular_file()) {
        std::cerr << "Attempting to write to existing file!" << std::endl;
        std::abort();
    }

    // Begin opening and processing r16
    std::ifstream inFile(fnd.path(), std::ios::binary | std::ios::ate);
    if(!inFile.is_open()) {
        std::cerr << "Failed to open input .r16!" << std::endl;
        std::abort();
    } 

    // Get input file size and determine map resolution (typical 2048x2048 for SC) 
    size_t iFileSize = inFile.tellg();
    inFile.seekg(0);
    uint16_t* iBuf = static_cast<uint16_t*>(std::malloc(iFileSize));
    inFile.read((char*)iBuf, iFileSize);

    // Need to add a endianness check here, the iBuffer needs correction due to the maps having
    // big endianness
    for(int i = 0; i < iFileSize/sizeof(uint16_t); i++) {
        iBuf[i] = (iBuf[i] << 8) + (iBuf[i] >> 8);
    }

    int nIBufMembers = iFileSize/sizeof(uint16_t);
    int mapR = sqrt(nIBufMembers);
    
    std::cout << "Displacement map (.r16) resolution: " << mapR << "x" << mapR << std::endl;

    // Process iBuf to pixel buffer
    unsigned char* pixelBuf = static_cast<unsigned char*>(std::malloc(nIBufMembers*4*sizeof(char)));

    // uint16 value range: 0-65535
    // unsigned char value range: 0-255
    // int16 value range: -32768-32767
    // char value range: -128-127

    for(int i = 0; i < nIBufMembers; i++) {
        char absRGB = (iBuf[i] / 256) % 256; // Move to range then clamp
        calcContourColor(iBuf[i], absRGB, pixelBuf[i*4], pixelBuf[i*4+1], pixelBuf[i*4+2], true);
        pixelBuf[i*4+3] = 255;  // A
    }

    // Output to specified directory
    std::string oPath;
    if(ond.is_directory()) {
        oPath = ond.path().string() + "\\" + fnd.path().filename().string() + ".png";
    } else {
        oPath = ond.path().string();
    }
    
    std::cout << "Output file name: " << oPath << std::endl;

    SOIL_save_image(oPath.c_str(), SOIL_SAVE_TYPE_PNG, mapR, mapR, 4, static_cast<unsigned char*>(pixelBuf));

    return 0;
}