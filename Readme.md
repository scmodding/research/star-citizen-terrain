# Star Citizen Terrain

This repository is a program being used for research into how Star Citizen uses the various terrain files (whether they be r16 displacement maps, or biome textures) in the generation of it's planetary terrain.

It will have several functions, ranging from CPU computation of the final composite map they use for their shader and map export, to eventually a small targetted renderer for visually showing the result of the process.

## Documentation

This section documents some of the basic formats and processes used in the terrain files, it may eventually be moved to a dedicated `./docs/` folder.

### R16

R16 is a commonly used terrain displacement map format, which is made up entirely of 16b integers. I have yet to find an example that is not a square buffer as well, so the assumption is that the displacement resolution is $r^{2}$ where $r = \frac{f}{2}$ where $f$ is the file size. 

The png output is current being compressed down into a uint8 range, yet curiously it seems that normal range translation calculations are ineffective on it, where merely using a modulus does give the right result going by _displ dds files in the P4K. This is caused by an endianness issue, all of the heightmaps are in big endian rather than little endian.

### Planetary description files (.pla)

Planetary description files appear to be a mostly unserialized format in Planet Tech V4, instead acting as a container for CryXMLB files which describe the location of and specific properties of colors in the planetary climate buffer referenced. There's several other large buffers present in the XML which are not unexpected for properly handling PBR with the planets and their changing atmospheres, these being the LUTs. Strangely I've also seen some duplication of the planetary _splat buffer here as well, though I am unsure as to the purpose of this.

## Building

CMake is required to configure and build this project, you can acquire it from the [CMake site](https://cmake.org/download/) for Windows and OSX, or from your package manager on Linux.

### Clang/LLVM & Ninja

In order to build using Clang/LLVM & Ninja you need to download both these dependencies.

#### Windows

Clang is best acquired from the main LLVM Git repos' [release page](https://github.com/llvm/llvm-project/releases/tag/llvmorg-16.0.0). For Windows you are likely looking for the  [LLVM-16.0.0-win64.exe](https://github.com/llvm/llvm-project/releases/download/llvmorg-16.0.0/LLVM-16.0.0-win64.exe) artifact.

To install Ninja you can either get it through the [Chocolatey Package Manager](https://chocolatey.org/install) by invoking `choco install ninja`, or you can manually download Ninja from [here](https://github.com/ninja-build/ninja/releases) and add it to your path.

#### Linux

In order to install these dependencies on Linux all you need to do is install the required packages from your package manager.

These should be the `clang` and `ninja` packages respectively.

### Configuration

Configuring CMake on the command line is fairly easy following this format: `cmake -S <repo_root_dir> -B <build_directory> -G <desired_build_gen> -D<CMakeVars>`

The root repository directory is fairly self explanatory, it's `./` relative to where you cloned the repository, or the path to the base of it.

The build directory is also straightforward, `./bin/` is a provided default, however it can be built anywhere.

CMake is not a build system itself, as much as it is a descriptor for build systems. As such you need to define which build system you are generating for with the desired build generator option. I recommend `Ninja` due to the fact that it's portable, multiplatform, and very performant. It's also what this repository is tested against. However you could use whatever build generator you desire or are used to.

All additional CMake variables you may want to configure can be found here, though I don't recommend tweaking them beyond `CMAKE_BUILD_TYPE`. Documentation on variables can be found here: [cmake-variables](https://cmake.org/cmake/help/latest/manual/cmake-variables.7.html).

### Build

After the configuration step is done, just invoke `cmake --build <build_directory>` where ever you targetted the build directory to.

Following this the binary will be in the root of the specified build directory.

## Issues

The CMakeLists needs some refinement

I'm currently using SOIL2, a derivative of STB_image as the means of exporting displacement map data, and eventually for pulling in dds files and rendering the final maps. However, it's specifically pretty terrible for exporting the heightmaps as there's a significant amount of resolution crunch from precision loss as SOIL2 only supports char* RGBA texture buffers for export, limiting the apparent resolution. Need to look into using libpng as export to maintain resolution.

## Acknowledgements

This project is not endorsed by or affiliated with the Cloud Imperium or Roberts Space Industries group of companies.
All game content and materials are copyright Cloud Imperium Rights LLC and Cloud Imperium Rights Ltd..  Star Citizen®,
Squadron 42®, Roberts Space Industries®, and Cloud Imperium® are registered trademarks of Cloud Imperium Rights LLC.
All rights reserved.

## Copyright notice

Copyright (c) 2023, Travis Law

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
